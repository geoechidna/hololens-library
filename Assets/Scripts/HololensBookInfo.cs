﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensBookInfo : MonoBehaviour {

	public string author, book;
	public HololensUiElem titleO, authorO;

	public GameObject waypoint, bookShelf;

	// Use this for initialization
	void Start () {
		titleO = transform.Find("BookTitle").gameObject.GetComponent<HololensUiElem>();
		authorO = transform.Find("Author").gameObject.GetComponent<HololensUiElem>();
		waypoint = GameObject.Find("Waypoint").transform.GetChild(0).gameObject;
		waypoint.SetActive(true);
		bookShelf = GameObject.FindGameObjectWithTag("Target");
		bookShelf.GetComponent<MeshRenderer>().material = Resources.Load<Material>("DarkWoodSL Glow");
	}
	
	// Update is called once per frame
	void Update () {
		titleO.text = book;
		authorO.text = author;
	}

	void OnDestroy()
	{
		waypoint.SetActive(false);
		bookShelf.GetComponent<MeshRenderer>().material = Resources.Load<Material>("DarkWoodSL");
	}
}
