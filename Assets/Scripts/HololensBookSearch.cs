﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensBookSearch : MonoBehaviour {

	public HololensUiElem query, result1, result2, result3, result4, result5, result6, pageNum, prev, next;
	public int page;

	public List<KeyValuePair<string, string>> results;

	// Use this for initialization
	void Start () {
		query = gameObject.transform.Find("Query").GetComponent<HololensUiElem>();
		result1 = gameObject.transform.Find("Result1").GetComponent<HololensUiElem>();
		result2 = gameObject.transform.Find("Result2").GetComponent<HololensUiElem>();
		result3 = gameObject.transform.Find("Result3").GetComponent<HololensUiElem>();
		result4 = gameObject.transform.Find("Result4").GetComponent<HololensUiElem>();
		result5 = gameObject.transform.Find("Result5").GetComponent<HololensUiElem>();
		pageNum = gameObject.transform.Find("Page").GetComponent<HololensUiElem>();
		prev = gameObject.transform.Find("Prev").GetComponent<HololensUiElem>();
		next = gameObject.transform.Find("Next").GetComponent<HololensUiElem>();

		page = 1;

		results = new List<KeyValuePair<string, string>>();

		var lastSearch = GameObject.Find("lastSearch").transform.GetChild(0).name;

		Debug.Log("Searched " + lastSearch);

		query.text = lastSearch;

		if(lastSearch.Contains("CAT") || lastSearch.Contains("PET"))
		{
			results.Add(new KeyValuePair<string, string>("Cats for Dummies","Paul Diddy"));
			results.Add(new KeyValuePair<string, string>("Big Cats","Leon Heart"));
			results.Add(new KeyValuePair<string, string>("Cats in the Cradle","Harry Chapin"));
			results.Add(new KeyValuePair<string, string>("The Domestic Cat","Dennis Turner"));
		}

		if(lastSearch.Contains("DOG") || lastSearch.Contains("PET"))
		{
			results.Add(new KeyValuePair<string, string>("Wild Dogs","Sean Gunn"));
			results.Add(new KeyValuePair<string, string>("Dogs for Dummies","Paul Diddy"));
		}


		if((lastSearch.Contains("DOG") && lastSearch.Contains("CAT")) || lastSearch.Contains("PET"))
		{
			results.Add(new KeyValuePair<string, string>("Spotless Pet","Steph Lush"));
			results.Add(new KeyValuePair<string, string>("Need to Know About Animals","Lisa Fox"));
			results.Add(new KeyValuePair<string, string>("An Encyclopedia of Domestic Animals","Sam"));
		}

	}
	
	// Update is called once per frame
	void Update () {
		var pages = (results.Count - 1) / 5 + 1;
		pageNum.text = page + "/" + pages;

		prev.gameObject.SetActive(page > 1);
		next.gameObject.SetActive(page < pages);

		var idx = (page - 1) * 5;
		var enabled = results.Count > idx;
		// result1.gameObject.SetActive(enabled);
		if(enabled)
		{
			result1.text = results[idx].Key + ", by " + results[idx].Value + " (Non-fiction)";
		}
		else
		{
			result1.text = "No results found";
		}


		idx++;
		enabled = results.Count > idx;
		result2.gameObject.SetActive(enabled);
		if(enabled)
		{
			result2.text = results[idx].Key + ", by " + results[idx].Value + " (Non-fiction)";
		}


		idx++;
		enabled = results.Count > idx;
		result3.gameObject.SetActive(enabled);
		if(enabled)
		{
			result3.text = results[idx].Key + ", by " + results[idx].Value + " (Non-fiction)";
		}


		idx++;
		enabled = results.Count > idx;
		result4.gameObject.SetActive(enabled);
		if(enabled)
		{
			result4.text = results[idx].Key + ", by " + results[idx].Value + " (Non-fiction)";
		}


		idx++;
		enabled = results.Count > idx;
		result5.gameObject.SetActive(enabled);
		if(enabled)
		{
			result5.text = results[idx].Key + ", by " + results[idx].Value + " (Non-fiction)";
		}


	}
}
