﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensBorrowed : MonoBehaviour {

	public HololensUiElem bookName, returnBy;

	// Use this for initialization
	void Start () {
		bookName = transform.Find("BookName").gameObject.GetComponent<HololensUiElem>();
		returnBy = transform.Find("ReturnBy").gameObject.GetComponent<HololensUiElem>();
		if(GameObject.Find("lastBook") == null)
		{
			bookName.text = "A Brief History of Time";
		}
		else{
            bookName.text = GameObject.Find("lastBook").transform.GetChild(0).name;
		}
		returnBy.text = "Please return by: " + System.DateTime.Now.AddDays(14).ToShortDateString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
