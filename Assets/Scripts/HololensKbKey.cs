﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensKbKey : HololensUiElem {

	HololensTextField textField;

	new void Start(){
		base.Start();
		textField = gameObject.transform.parent.gameObject.GetComponentInChildren<HololensTextField>();
	}

    public override void OnClick(){
		base.OnClick();

		if(text == "Home")
		{
			GameObject.FindObjectOfType<HololensManager>().ChangeScene("MainMenu");
		}

        if (text.Length == 1)
            textField.text += text;
        if (text.Contains("<-"))
            textField.text = textField.text.Substring(0, textField.text.Length - 1);
        if (text == "Space")
            textField.text += " ";
        if (text == "Search")
        {
            GameObject lastSearch = GameObject.Find("lastSearch");
            if (lastSearch == null)
            {
                lastSearch = new GameObject("lastSearch");
                new GameObject(textField.text).transform.SetParent(lastSearch.transform);
            }
            else
            {
                lastSearch.transform.GetChild(0).name = textField.text;
            }
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("BookSearch");
        }
        if(text == "Back to Results")
        {
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("BookSearch");
        }

        if(text == "Borrow" || text == "Borrow a book")
        {
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("Borrowing");
        }


        if (text == "Search for a book" || text == "Search Again")
        {
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("Keyboard");
			GameObject lastSearch = GameObject.Find("lastSearch");
            if (lastSearch != null)
            {
                GameObject.Find("TextField").GetComponent<HololensTextField>().initial = lastSearch.transform.GetChild(0).name;
            }
        }

        if (text == "View map")
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("Minimap");

		if (text == "Next")
            GameObject.FindObjectOfType<HololensBookSearch>().page++;

        if (text == "Prev")
            GameObject.FindObjectOfType<HololensBookSearch>().page--;

        if (gameObject.name.Contains("Result"))
        {
			var strings = text.Split(new string[]{", by ", " ("}, System.StringSplitOptions.None);
			var book = strings[0];
			var author = strings[1];            
            
            GameObject lastBook = GameObject.Find("lastBook");
            if (lastBook == null)
            {
                lastBook = new GameObject("lastBook");
                new GameObject(book).transform.SetParent(lastBook.transform);
                new GameObject(author).transform.SetParent(lastBook.transform);
            }
            else
            {
                lastBook.transform.GetChild(0).name = book;
                lastBook.transform.GetChild(1).name = author;
            }

            GameObject.FindObjectOfType<HololensManager>().ChangeScene("BookInfo");

            FindObjectOfType<HololensBookInfo>().author = author;
            FindObjectOfType<HololensBookInfo>().book = book;
        }
        if(text == "Back to book")
        {
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("BookInfo");

            FindObjectOfType<HololensBookInfo>().author = GameObject.Find("lastBook").transform.GetChild(1).name;
            FindObjectOfType<HololensBookInfo>().book = GameObject.Find("lastBook").transform.GetChild(0).name;
        }

        if(text == "Help")
        {
            GameObject.FindObjectOfType<HololensManager>().ChangeScene("Help");
        }
    }

}
