﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensManager : MonoBehaviour {
    
    GameObject hlPanel, crosshair, book;

    public float distance = 0.8f;
    public Vector3 angleFromPlayer = new Vector3();

public Vector3 bookPos;
    public string lastSearch = "";

	// Use this for initialization
	void Start () {
        hlPanel = Instantiate(Resources.Load<GameObject>("hlPanel"));
        crosshair = Instantiate(Resources.Load<GameObject>("Sphere"));
        crosshair.transform.SetParent(transform, false);
        crosshair.transform.localPosition = new Vector3(0,0,0.7f);
        crosshair.transform.localScale = new Vector3(0.005f, 0.005f, 0.005f);
        hlPanel.SetActive(false);
        crosshair.SetActive(false);
        distance = 0.8f;

        book = GameObject.Find("book");
        bookPos = book.transform.localPosition;
        book.SetActive(false);


        // var g = GameObject.Instantiate(Resources.Load<GameObject>("Keyboard"));
        // g.transform.SetParent(hlPanel.transform, false);

        ChangeScene("MainMenu");

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
            ChangeScene("MainMenu");

        if(Input.GetKey(KeyCode.LeftShift))
        {
            book.transform.localPosition = new Vector3(0, -0.1f, 1f);
            book.transform.localRotation = Quaternion.Euler(0,-90,0);
        }

        if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            book.transform.localPosition = bookPos;
            book.transform.localRotation = Quaternion.Euler(0,0,0);
        }

        if(Input.GetKeyDown(KeyCode.LeftControl))
        book.SetActive(!book.activeInHierarchy);

        if (Input.GetMouseButtonDown(0))
        {
            Debug.DrawRay(transform.position, transform.forward, Color.red, 10);
            RaycastHit[] hits = Physics.RaycastAll(transform.position, transform.forward);
            foreach (var hit in hits)
            {
                var tx = hit.transform;
                var p = tx.parent;
                if (p == null) continue;
                p = p.parent;
                if (p == null) continue;
                var g = p.gameObject;
                var ui = g.GetComponentInChildren<HololensUiElem>();
                if (ui != null)
                {
                    ui.OnClick();
                }
            }
        }


		if(Input.GetMouseButtonDown(1))
        {
            hlPanel.SetActive(!hlPanel.activeInHierarchy);
            hlPanel.transform.position = transform.position + new Vector3(transform.position.x, 0, transform.position.z).normalized * distance;
            angleFromPlayer = transform.forward;
            crosshair.SetActive(hlPanel.activeInHierarchy);
        }

        if (hlPanel.activeInHierarchy)
        {

            Vector3 playerDir = transform.forward;

            float rotation = Vector3.Angle(playerDir, angleFromPlayer);
            float laxAngle = 30f;
            if (Mathf.Abs(rotation) >= laxAngle)
                angleFromPlayer = Vector3.MoveTowards(angleFromPlayer, playerDir, 1.0f * Time.deltaTime);
            
            hlPanel.transform.position = transform.position + angleFromPlayer.normalized * distance;
            //hlPanel.transform.position = Vector3.MoveTowards(hlPanel.transform.position, hlPanel.transform.position - transform.position, (hlPanel.transform.position - transform.position).magnitude );
            //var p = hlPanel.transform.position;
            ////p.y = transform.position.y - 0.5f;
            //p.y = transform.position.y;
            //hlPanel.transform.position = p;
            //Vector3 target = transform.position + transform.forward * distance;
            //target.y = Mathf.Clamp(target.y, transform.position.y - 1, transform.position.y + 1);


            //hlPanel.transform.position = target;

            hlPanel.transform.LookAt(transform);
            hlPanel.transform.Rotate(90, 0, 0);
        }
    }

    public void ChangeScene(string NewScene)
    {
        

        foreach (Transform child in hlPanel.transform)
        {
            GameObject.Destroy(child.gameObject);
        }

        if (NewScene == "MainMenu")
        {
            var g = GameObject.Find("lastSearch");
            if (g != null)
            {
                Destroy(g);
            }
        }

        if(NewScene != "MainMenu")
            Instantiate(Resources.Load<GameObject>("Home button")).transform.SetParent(hlPanel.transform, false);

        Instantiate(Resources.Load<GameObject>(NewScene)).transform.SetParent(hlPanel.transform, false);
    }
}
