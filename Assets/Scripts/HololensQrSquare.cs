﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensQrSquare : HololensUiElem {

	System.DateTime timeout;
	bool counting = false;

	GameObject player;

	// Use this for initialization
	void Start () {
		base.Start();
		counting = false;
        player = GameObject.Find("MainCamera");

		if(GameObject.Find("lastBook") == null)
			base.transform.Find("GameObject (5)").gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		base.Update();

		bool foundBox = false;
		bool foundBook = false;

		var hits = Physics.RaycastAll(player.transform.position, player.transform.forward);

		foreach(var hit in hits)
		{
			if(hit.collider.gameObject.name == "QrCollider")
			{
				foundBox = true;
			}
			if(hit.collider.gameObject.name == "book")
			{
				foundBook = true;
			}
		}

		if(foundBox && foundBook)
		{
			if(counting == false)
			{
				timeout = System.DateTime.Now.AddSeconds(2);
				counting = true;
			}

			if(System.DateTime.Now > timeout)
			{
            	GameObject.FindObjectOfType<HololensManager>().ChangeScene("Borrowed");
			}
		}
		else
		counting = false;
	}
}
