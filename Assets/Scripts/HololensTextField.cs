﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensTextField : HololensUiElem {

	public string initial = "";

void Start()
{
	base.Start();
	base.tm.offsetZ = -0.5f;
	if(initial.Length!=0)
	base.text = initial;
}

protected void Update(){
		base.Update();
		base.tm.anchor = TextAnchor.MiddleLeft;
		base.tm.transform.localPosition = new Vector3(base.backgroundSize.x / 2 - 0.4f, 0.1f	);
}

}
