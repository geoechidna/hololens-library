﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensUiElem : MonoBehaviour{

	public string text;

	public Vector3 backgroundSize;

	protected GameObject background;

	protected TextMesh tm;

	public virtual void OnClick(){
		Debug.Log("Pressed " + text);
	}

	protected void Start(){
		background = Instantiate(Resources.Load<GameObject>("uiElem"));
		background.transform.SetParent(transform, false);
		background.transform.localRotation = Quaternion.Euler(90,0,0);
		// transform.localPosition = new Vector3(1.6f * transform.localPosition.x, transform.localPosition.y, 2*transform.localPosition.z);
		tm = background.GetComponentInChildren<TextMesh>();
		// tm.anchor = TextAnchor.MiddleLeft;
		// tm.transform.localPosition = new Vector3(-backgroundSize.x / 2, 0);
        // transform.localScale = new Vector3(1 / transform.parent.lossyScale.x, 1 / transform.parent.lossyScale.y, 1 / transform.parent.lossyScale.z) / 10;
        // transform.localPosition = new Vector3(transform.GetChild(0).localPosition.x, transform.GetChild(0).localPosition.y, transform.GetChild(0).transform.localPosition.z);
        // transform.localScale = new Vector3(transform.localScale.x * transform.GetChild(0).localScale.x,
       	// 									transform.localScale.y * transform.GetChild(0).localScale.y,
        // 									transform.localScale.z * transform.GetChild(0).localScale.z);
	}

	protected virtual void Update(){
		background.transform.GetChild(0).localScale = backgroundSize;
		tm.text = text;
	}

}
