﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapIcon : MonoBehaviour {

	GameObject arrow;

    // Use this for initialization
    void Start()
    {
		arrow = Instantiate(Resources.Load<GameObject>("Minimap Arrow"));
		arrow.transform.localScale = Vector3.one * 3;
    }

    // Update is called once per frame
    void Update()
    {
		arrow.transform.position = new Vector3(transform.position.x, transform.position.y + 10, transform.position.z);
		arrow.transform.rotation = Quaternion.Euler(90,0,180-transform.rotation.eulerAngles.y);
    }
}
